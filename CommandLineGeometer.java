//******************************************************
// Honor Code: This work is mine unless otherwise stated.
// Cassie Bachik and Sydney Fernandez
// 3/9/17
// Lab 7
// Purpose: To calculate the area and volumes of geometerical shapes
//******************************************************


import java.util.Date;
import java.util.Scanner;

public class CommandLineGeometer
{

    private enum GeometricShape { sphere, triangle, cylinder };

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        GeometricShape shape = GeometricShape.sphere;
//varibale
        double radius;
        double height;
        double base;
        int x;
        int y;
        int z;
// welcoming them to the program
        System.out.println("Cassie Bachik and Sydney Fernandez " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();
//ask them to enter the radius
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();
//calculating the volume and radius
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);
        System.out.println("The volume is equal to " + sphereVolume);
        System.out.println();

        shape = GeometricShape.triangle;
//asking for the length of each side of the triangle
        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("Calculating the area of a " + shape);
        double triangleArea = GeometricCalculator.calculateTriangleArea(y, x);
        System.out.println("The area is equal to " + triangleArea);
        System.out.println();

        shape = GeometricShape.cylinder;
// calculating for the cylinder
        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);
        System.out.println("The volume is equal to " + sphereVolume);
        System.out.println();
    }
}
