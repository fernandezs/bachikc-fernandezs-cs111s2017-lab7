
import java.text.DecimalFormat;

public class GeometricCalculator {



    public static double calculateSphereVolume(double radius) {
        double volume;
        volume = (4/ 3) * (Math.PI) * radius * radius * radius;
        return volume;
    }

    public static double calculateTriangleArea(double base, double height) {
        double area;
        area = (1/2)* base* height;
        return area;
    }

    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * radius * radius * height;
        return volume;

   
    }
}
